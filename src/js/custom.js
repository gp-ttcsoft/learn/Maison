$(function () {

    /*==========FOR SVG ==============*/
    // svg4everybody();
    /*FOR MENU*/
    $('.burger').click(function (e) {
        e.preventDefault();
        $(this)
            .find('.icon')
            .toggleClass('menu')
            .toggleClass('close');

        $('#menu').toggleClass('open');
        if ($('.header-menu').hasClass('open')) {
            document.ontouchmove = function(e){ e.preventDefault(); }
            $('body').css('overflow', 'hidden');
        } else {
            document.ontouchmove = function(e){ return true; }
            $('body').css('overflow', 'auto');
        }
        $('body').toggleClass('menu_open');

        $('.header-back').toggleClass('open');
    });

    // $('#menu').swipe({
    //     swipeRight: function () {
    //         $('#menu').removeClass('open');
    //         $('.header-back').removeClass('open');
    //         $('body').css('overflow', 'auto');
    //         $('.burger').find('.icon').toggleClass('close');
    //         $('.burger').find('.icon').addClass('menu');
    //     }
    // });
    //
    // $('.panel__menu').swipe({
    //     swipeLeft: function () {
    //         $('#menu').addClass('open');
    //         $('.header-back').addClass('open');
    //         $('body').css('overflow', 'hidden');
    //         $('.burger').find('.icon').addClass('close');
    //         $('.burger').find('.icon').removeClass('menu');
    //     }
    // });
    //
    // $('.header-back').click(function (e) {
    //     e.preventDefault();
    //     $(this).removeClass('open');
    //     $('.burger')
    //         .find('.icon')
    //         .removeClass('menu')
    //         .addClass('close');
    //
    //     $('#menu').removeClass('open');
    //     $('body').removeClass('menu_open');
    //     $('.icon').removeClass('close');
    //     $('body').css('overflow', 'hidden');
    // });


    // /*VALIDATE*/
    //     $(".js-form--top").validate({
    //         submitHandler: function(element) {
    //             $(".js-form--top").addClass('validate');
    //             $('#modal-thanks').addClass('open');
    //         }
    //     });
    //
    // $(".js-form--bottom").validate({
    //     submitHandler: function(element) {
    //         $(".js-form--bottom").addClass('validate');
    //         $('#modal-thanks').addClass('open');
    //     }
    //
    // });
    //
    // $(".js-form--call").validate({
    //     submitHandler: function(element) {
    //         $(".js-form--call").addClass('validate');
    //         $('#modal-thanks').addClass('open');
    //     }
    //
    // });


    /*----------------------------------------
     TRANSITION SCROLL
     ----------------------------------------*/
    // $('.scroll').on("click", function(e) {
    //     // e.preventDefault();
    //     var anchor = $(this);
    //     $('html, body').stop().animate({
    //         scrollTop: $(anchor.attr('href')).offset().top
    //     }, 1000);
    // });

    // slider
    $('.slider').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows:true,
        dots: false,
        autoplay: false,
        autoplaySpeed: 6000,
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    arrows:false,
                    dots:true
                }
            },
        ]
    });

    $(".slider").on('afterChange', function(event, slick, currentSlide){
        $("#counter").text(currentSlide + 1);
    });

    /*----------------------------------------
	RANGE SLIDER ION
----------------------------------------*/
    var range = $('#range'),
        minInput = $('#range-min'),
        maxInput = $('#range-max'),
        minValue = 0,
        maxValue = 200;


    range.ionRangeSlider({
        type: "double",
        min: minValue,
        max: maxValue,
        grid: true,
        step: 1,
        hide_min_max: true,
        hide_from_to: true,
        grid_snap: false,
        onStart: function (data) {
            minInput.prop("value", data.from);
            maxInput.prop("value", data.to);
        },
        onChange: function (data) {
            minInput.prop("value", data.from);
            maxInput.prop("value", data.to);
        }
    });

    var instance = range.data("ionRangeSlider");


    minInput.on("change keyup", function () {
        var val = $(this).prop("value");

        // validate
        if (val < minValue) {
            val = minValue;
        } else if (val > maxValue) {
            val = maxValue;
        }

        instance.update({
            from: val
        });
    });

    maxInput.on("change keyup", function () {
        var val = $(this).prop("value");

        // validate
        if (val > maxValue) {
            val = maxValue;
        } else if (val < minValue) {
            val = minValue;
        }

        instance.update({
            to: val
        });
    });

//select
    $('select').selectBox({
        mobile: true,
        menuSpeed: 'fast'
    });

    //filter-mobile

    $('#js-filter').click(function (e) {
        e.preventDefault();
        $(".aside")
            .toggleClass('open');

        // $('#js-filter').toggleClass('open');
        // if ($('.aside').hasClass('open')) {
        //     document.ontouchmove = function(e){ e.preventDefault(); }
        //     $('body').css('overflow', 'hidden');
        // } else {
        //     document.ontouchmove = function(e){ return true; }
        //     $('body').css('overflow', 'auto');
        // }
        // $('body').toggleClass('menu_open');
        //
        // $('.header-back').toggleClass('open');
    });

});


